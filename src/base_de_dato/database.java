/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package base_de_dato;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 *
 * @author Michael
 */
public class database {
    // se declara la  variable para la conexion y se llama al metodo
    Connection conexion = null;
    String password = "admin"; //contraseña de base de datos
    String user = "root";// usuario de base de datosº
    String url = "jdbc:mysql://localhost:3306/muebleria?serverTimezone=UTC";
    
    
    public Connection conexion() {
        try {
            conexion = DriverManager.getConnection(url, user, password);
            //JOptionPane.showMessageDialog(null, "Conexion realizada");
        } catch (SQLException ex) {
            //System.out.println("Error en la conexion a la BD : " + ex);
        }
        return conexion;
    }

    public void guardar_datos(String codigo_muen, Date fecha,String nombre, String despcripcion, double tamanio, String color, String pieza, String categoria, int stock, double precio) 
    {
        conexion(); 
        int registros;
        PreparedStatement sentencia = null;
        String sentenciaSQL = "INSERT INTO inventario (cod_mueble, fech_ingre_mueble, nom_mueb, des_mueble,tam_mueble, col_mueble, piez_mueble ,cate_mueble, sto_mueble, pre_mueble ) VALUES (?,?,?,?,?,?,?,?,?,?)";
      
        try { 
            sentencia = conexion.prepareStatement(sentenciaSQL);
            sentencia.setString(1, codigo_muen);
            sentencia.setDate(2, fecha);
            sentencia.setString(3, nombre);
            sentencia.setString(4, despcripcion);
            sentencia.setDouble(5, tamanio);
            sentencia.setString(6, color);
            sentencia.setString(7, pieza);
            sentencia.setString(8, categoria);
            sentencia.setInt(9, stock);
            sentencia.setDouble(10, precio);
            registros = sentencia.executeUpdate();
            JOptionPane.showMessageDialog(null, "REGISTRO COMPLETO");
            
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "ERROR EN EL REGISTRO: " + ex);
        }

            
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    
    
    }
    //este metodo permite 
    public void actualizar_datos(String codigo_muen, Date fecha, String nombre, String despcripcion, Double tamanio,
            String color, String pieza, String categoria, int stock, Double precio) {
        
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
         conexion(); 
        int registros;
        PreparedStatement sentencia = null;
        String sentenciaSQL = ("UPDATE inventario SET fech_ingre_mueble = ?, nom_mueb = ?, des_mueble = ?,tam_mueble = ?, col_mueble = ?, piez_mueble = ?,cate_mueble = ?, sto_mueble = ?, pre_mueble = ? WHERE cod_mueble =?");
      
        try { 
            sentencia = conexion.prepareStatement(sentenciaSQL);
            
            sentencia.setDate(1, fecha);
            sentencia.setString(2, nombre);
            sentencia.setString(3, despcripcion);
            sentencia.setDouble(4, tamanio);
            sentencia.setString(5, color);
            sentencia.setString(6, pieza);
            sentencia.setString(7, categoria);
            sentencia.setInt(8, stock);
            sentencia.setDouble(9, precio);
            sentencia.setString(10,codigo_muen);
           
            registros = sentencia.executeUpdate();
            JOptionPane.showMessageDialog(null, "Actualizacion completada");
            
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "ERROR EN EL ACTUALIZACION EL CODIGO NO DEBE CAMBIAR: " + ex);
        }
    }

    
  
}
